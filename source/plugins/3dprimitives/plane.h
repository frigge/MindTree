#ifndef MT_GEO_PLANE_H
#define MT_GEO_PLANE_H

#include "../datatypes/Object/object.h"

namespace prim3d
{
    GeoObjectPtr createPlane(float scale);
}

#endif
