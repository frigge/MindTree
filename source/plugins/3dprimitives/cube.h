#ifndef PRIM_CUBE
#define PRIM_CUBE

#include "../datatypes/Object/object.h"

namespace prim3d
{
    GeoObjectPtr createCube(float scale);
};
#endif
